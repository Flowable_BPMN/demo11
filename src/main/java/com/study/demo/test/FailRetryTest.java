package com.study.demo.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.study.demo.config.BaseConfiguation;


/**
 *失败重试--测试
 *
 */
public class FailRetryTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("failretryprocess")
													.name("failretryprocess")
													.addClasspathResource("process/失败重试.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例 
	 * 
	 */
	@Test
	public void start() {
		String processDefinitionKey = "failretryprocess";
    	

        Map<String, Object> vars = new HashMap<String, Object>();
		runtimeService.startProcessInstanceByKey(processDefinitionKey,vars);
	}
	
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "17507";
		taskService.complete(taskId);
	}
	/**
	 * 睡眠20分钟
	 * @throws InterruptedException 
	 */
	@Test
	public void sleep() throws InterruptedException {
		Long millis = 1200000L;
		Thread.sleep(millis);
	}
}
