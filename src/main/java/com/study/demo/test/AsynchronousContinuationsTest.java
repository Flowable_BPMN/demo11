package com.study.demo.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.study.demo.config.BaseConfiguation;


/**
 *异步延续--测试
 *
 */
public class AsynchronousContinuationsTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("asynchronouscontinuationsprocess")
													.name("asynchronouscontinuationsprocess")
													.addClasspathResource("process/异步延续.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例 
	 * 
	 */
	@Test
	public void start() {
		String processDefinitionKey = "asynchronouscontinuationsprocess";
    	

        Map<String, Object> vars = new HashMap<String, Object>();
		runtimeService.startProcessInstanceByKey(processDefinitionKey,vars);
	}
	
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "2505";
		taskService.complete(taskId);
	}
	/**
	 * 睡眠5分钟
	 * @throws InterruptedException 
	 */
	@Test
	public void sleep() throws InterruptedException {
		Long millis = 300000L;
		Thread.sleep(millis);
	}
}
