package com.study.demo.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 生成发票
 *
 */
public class GenerateInvoiceJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		System.out.println("===================生成发票=================");

	}

}
