package com.study.demo.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 失败重试3次
 *
 */
public class ServieTwoJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		System.out.println("===================排他作业【Two】=================");
		
	}

}
