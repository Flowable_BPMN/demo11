package com.study.demo.delegate;

import org.flowable.engine.delegate.BpmnError;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 失败重试3次
 *
 */
public class FailRetryJavaDelegate implements JavaDelegate {
	private int i =0;

	public void execute(DelegateExecution execution) {
		i ++ ;
		System.out.println("===================失败重试3次,这是第【"+ i + "次】重试=================");
		
		throw new BpmnError("失败重试,i=" + i);
	}

}
